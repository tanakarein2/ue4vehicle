
#pragma once
#define _USE_MATH_DEFINES

namespace mapping
{
struct Waypoint
{

    //Id of this Waypoint object
    int waypoint_id = 0;
    //Id of Vector3 that represents the position of this waypoint
    int point_id = 0;
    //reference velocity of this waypoint.[km / h]
    float velocity = 0.f;
    //describes whether vehicle must stop at this waypoint
    //no_stop = 0, stop = 1
    int stop_line = 0;
    //distance to left border of the belonging lane in[m]
    float left_width = 0.f;
    //distance to right border of the belonging lane in[m]
    float right_width = 0.f;
    //height limit for the vehicle to drive this waypoint[m]
    float height = 0.f;

    Waypoint(const int waypoint_id = 0,
             const int point_id = 0,
             const float velocity = 0.f,
             const int stop_line = 0,
             const float left_width = 0.f,
             const float right_width = 0.f,
             const float height = 0.f)
    {
        this->waypoint_id = waypoint_id;
        this->point_id = point_id;
        this->velocity = velocity;
        this->stop_line = stop_line;
    }
};

struct Lane
{
    //This represents a lane in a map.

    //Id of this Lane object.Must be unique among all lane objects.
    int lane_id;

    //Id of the first waypoint that belongs to this lane
    int start_waypoint_id;

    //Id of the last waypoint that belongs to this lane
    int end_waypoint_id;

    //This describes how many lane_array there are in left side of this lane.
    //E.g.If there are 2 lane_array on the left side, then lane_number will be 2.
    //If the road is single lane, then this will be 0.
    //This will be always 0 in intersection.
    int lane_number;

    //Total number of lane_array present in road.
    int num_of_lanes;

    //Speed limit of this lane in[km / h]
    float speed_limit;

    //Length of this lane in[m]
    //i.e.accumulated length from start_waypoint to end_waypoint of this lane
    float length;

    //Maximum width of vehicle that can drive this lane in[m]
    float width_limit;

    //Maximum height of vehicle that can drive this lane in[m].
    float height_limit;

    //Maximum weight of vehicle that can drive this lane in[kg].
    float weight_limit;
};
}; // namespace mapping