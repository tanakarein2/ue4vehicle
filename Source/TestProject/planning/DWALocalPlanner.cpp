// Fill out your copyright notice in the Description page of Project Settings.

#include "DWALocalPlanner.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/math_define.h"
#include "define/trajectory_define.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "msgs/trajectory_msgs.h"
#include "config/vehicle_config.h"
#include <math.h>
#include <time.h>
DWALocalPlanner::DWALocalPlanner()
{
	cout << "create dwa planner" << endl;
}

DWALocalPlanner::~DWALocalPlanner()
{
}

void DWALocalPlanner::Start()
{
	predictedLoci.header = {0, time(NULL), "dwa local path", 0.f};
}
void DWALocalPlanner::SetState()
{
	//this->objPose.obj.poses = SensorMsgs::instance()->objects;
	this->getPlan.goal.position = NavigationMsgs::instance()->globalPath.target;
	this->base = VehicleStateMsgs::instance()->geometry;
	this->ackermannDrive = AckermannMsgs::instance()->ackermannDrive;
	this->state = {
		VehicleStateMsgs::instance()->geometry.pose.position.x,
		VehicleStateMsgs::instance()->geometry.pose.position.y,
		VehicleStateMsgs::instance()->geometry.pose.euler.z,
		AckermannMsgs::instance()->ackermannDrive.steering_angle};

	this->dt = dwa::DELTA_TIME; //VehicleStateMsgs::instance()->vehicleState.header.deltaTime;
	//this->objects = SensorMsgs::instance()->objects;
}
void DWALocalPlanner::SpinOnce()
{
	//ResetVector();
	MakePath();
	//evaluate_path(paths, goal, state, obstacle.position);
	
}
void DWALocalPlanner::GetState()
{
	predictedLoci.header.seq++;
	TrajectoryMsgs::instance()->loci = this->predictedLoci;
	if (isDebug)
	{
		predictedLoci.Debug();
	}
	predictedLoci.loci.clear();
}
void DWALocalPlanner::MakePath()
{
	float v1Range = dt * vehicle::MAX_ACCELERATION;
	float v2Range = vehicle::MAX_ANGULAR_VELOCITY; //dt * vehicle::MAX_ANGULAR_ACCELERATION;
	float v1 = ackermannDrive.speed;
	float v2 = ackermannDrive.steering_angle_velocity;
	float min_lin_velo = Math::Clamp(v1 - v1Range, vehicle::LIMIT_MIN_VELO);
	float max_lin_velo = Math::Clamp(v1 + v1Range, vehicle::LIMIT_MAX_VELO);
	float min_ang_velo = Math::Clamp(v2 - v2Range, vehicle::LIMIT_MIN_ANG_VELO);
	float max_ang_velo = Math::Clamp(v2 + v2Range, vehicle::LIMIT_MAX_ANG_VELO);
	float deltaLinearVelocity = (float)((max_lin_velo - min_lin_velo) / dwa::DELTA_LINEAR_VELOCITY_STEP);
	float deltaAngularVelocity = (float)((max_ang_velo - min_lin_velo) / dwa::DELTA_ANGULAR_VELOCITY_STEP);
	if (isDebug)
	{
		//float num_v1 = (max_lin_velo - min_lin_velo) / dwa::DELTA_LINEAR_VELOCITY;
		//float num_v2 = (max_ang_velo - min_ang_velo) / dwa::DELTA_ANGULAR_VELOCITY;
		//cout << "numv1:" << num_v1 << "numv2:" << num_v2 << endl;
		cout << "v1" << v1 << "v2" << v2 << endl;
		cout << "minV" << min_lin_velo << "maxV" << max_lin_velo << endl;
		cout << "minA" << min_ang_velo << "maxA" << max_ang_velo << endl;
	}
	//virtual v1 v2 for range

	//for (v2 = min_ang_velo; v2 <= max_ang_velo; v2 += deltaAngularVelocity)
	//{
	//	for (v1 = min_lin_velo; v1 <= max_lin_velo; v1 += deltaLinearVelocity)
	//	{
	float omega=min_ang_velo;
	float velo=min_lin_velo;
	for (int i=0;i<dwa::DELTA_ANGULAR_VELOCITY_STEP;i++)
	{
		for (int j=0;j<dwa::DELTA_LINEAR_VELOCITY_STEP;j++)
		{
			omega=min_ang_velo+deltaAngularVelocity*i;
			velo=min_lin_velo+deltaLinearVelocity*j;
			//virtual input of linear and angular
			PredictState(v1, v2);
		}
	}
}
void DWALocalPlanner::PredictState(float v1, float v2)
{
	Locus predictedLocus = {};
	float tmp_x = state.x;
	float tmp_y = state.y;
	float tmp_th = state.th;
	float tmp_phi = state.phi;
	dt = 0.0166f;
	v1 = v1 * 2.f;
	for (int i = 0; i < dwa::PREDICT_STEP; i++)
	{
		tmp_x += v1 * cos(tmp_th) * dt;
		tmp_y += v1 * sin(tmp_th) * dt;
		tmp_th += (tan(tmp_phi) * v1 * dt / vehicle::CAR_LENGTH);
		//tmp_phi += v2 * dt;
		State newState = {
			tmp_x,
			tmp_y,
			tmp_th,
			tmp_phi};
		predictedLocus.states.emplace_back(newState);
		cout << "x" << newState.x << "y" << newState.y << "phi" << tmp_phi << "th" << tmp_th << endl;
	}
	predictedLoci.loci.emplace_back(predictedLocus);
}
