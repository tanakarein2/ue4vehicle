// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#define _USE_MATH_DEFINES
#include "design/TemplateMethod.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/sensor_define.h"
#include "define/trajectory_define.h"
#include "define/ackermann_define.h"
#include <iostream>
#include <vector>
#include <map>
#include <math.h>
#include <time.h>
#include <array>
using std::array;
using std::cout;
using std::endl;
using std::map;
using std::vector;

#include "CoreMinimal.h"
namespace dwa
{
const int PREDICT_STEP = 10;
const float DELTA_LINEAR_VELOCITY = 5.f;
const float DELTA_ANGULAR_VELOCITY = float(M_PI * 15 / 180);
const float WEIGHT_ANGLE = 0.15f;
const float WEIGHT_VELOCITY = 0.14f;
const float WEIGHT_OBSTACLE = 0.15f;
const float DELTA_TIME = 0.0166f;
const int DELTA_LINEAR_VELOCITY_STEP = 3;
const int DELTA_ANGULAR_VELOCITY_STEP = 3;

} // namespace dwa

class DWALocalPlanner : public TemplateMethod
{
private:
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;

	bool isDebug = true;
	std::vector<Vector3> objects;
	//std::vector<Vector3> nearest_obs;
	//std::vector<float> dist_obst;
	Geometry base = {};
	AckermannDrive ackermannDrive = {};
	float dt;
	State state = {};
	Locus opt_path = {};
	GetPlan getPlan;
	Loci predictedLoci = {};
	ObjPose objPose;

	void ResetVector()
	{
		//paths.clear();
		//nearest_obs.clear();
		//opt_path.ResetVector();
	}
	void PredictState(float v1, float v2);
	void MakePath();
	
public:
	DWALocalPlanner();
	~DWALocalPlanner();
};
