#include "StateLatticeLocalPlanner.h"

#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/math_define.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "msgs/trajectory_msgs.h"
#include <iostream>
#include <time.h>
#include <vector>
#include <math.h>
using std::cout;
using std::endl;
using std::vector;
//=

#include "Math/Color.h"
#include "unreal/unreal_define.h"
#include "DrawDebugHelpers.h"

StateLatticeLocalPlanner::StateLatticeLocalPlanner()
{
    cout << "create Local Planner Class" << endl;
}
StateLatticeLocalPlanner::~StateLatticeLocalPlanner()
{
    cout << "del Local Planner Class" << endl;
}
void StateLatticeLocalPlanner::Start()
{
    localPath.header = {0, time(NULL), "local path", 0.f};
}

void StateLatticeLocalPlanner::SetState()
{
    this->localCostmap = NavigationMsgs::instance()->localCostmap;
    this->globalPath = NavigationMsgs::instance()->globalPath;
    this->geometry = VehicleStateMsgs::instance()->geometry;
}

void StateLatticeLocalPlanner::GetState()
{
    NavigationMsgs::instance()->localPath = localPath;
    if (isDebug)
    { 
        NavigationMsgs::instance()->localPath.Debug();
    }
}

void StateLatticeLocalPlanner::SpinOnce()
{
    /*ここでLocalとGlobalの使い分け
		条件は２つ
			1.ぶつかるかどうかー＞OccupancyGridにより判定->Plan変更
			2.waypointから離れすぎているかどうか->RecoveryMotionへ遷移
		*/
    /// wrench method選択
    localPath.target = EvaluateOccupancyGrid();
    localPath.header.seq++;
}

static float OverrideCost(float scale, float center, float currrentTarget)
{
    return scale * abs((center - abs(currrentTarget)));
}
static float MapCost(float scale, float cost)
{
    return scale * cost;
}

Vector3 StateLatticeLocalPlanner::EvaluateOccupancyGrid()
{
    int stepT = 10; //steps of scale
    int stepX = 2;
    int stepY = 2;
    float minCost = 10.f;
    int minY = 0;
    Vector3 minimumCost = 0;

    for (int y = 0; y < localCostmap.info.height; y += stepY)
    {
        float mapCost = 0.f;
        float currentCost = 0.f;
        for (int x = localCostmap.info.width*1/3; x < localCostmap.info.width; x += stepX)
        {
            //Indexがある限りはｘｙはGrid上の値
            int localCostmapIndex = TF::Grid2Array(localCostmap.info.width, x, y);
            mapCost += MapCost(occupancyScale, float(localCostmap.Data[localCostmapIndex]));
        }
        float overrideCost = OverrideCost(overrideScale, localCostmap.offset.y, y);
        currentCost += overrideCost;
        currentCost += mapCost;
        if (minCost > currentCost)
        {
            minCost = currentCost;
            minY = y;
        }
    }
    Vector2i avoidGrid = {localCostmap.info.width, minY};
    Vector2 pos = TF::Grid2Position(avoidGrid, localCostmap.offset, localCostmap.info.resolution);
    Vector3 localPoint = {pos.x, pos.y, 0.f};
    Vector3 worldPoint = TF::Local2WorldPosition(localCostmap.info.origin, localPoint);
    minimumCost = {worldPoint.x, worldPoint.y, minCost};

    return minimumCost;
}
