// Fill out your copyright notice in the Description page of Project Settings.
//============================================================================
// Name        : astar.cpp
// Author      : taiki tanaka
// Version     : v1
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "AstarNavigation.h"
#include "define/tf_define.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/math_define.h"
#include "define/sensor_define.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "msgs/sensor_msgs.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <map>

AstarNavigation::AstarNavigation(){
	cout<<"create astar"<<endl;
	motion=get_motion_model();
}

void AstarNavigation::Start(){
	localPath.header= {0, time(NULL), "local path", 0.f};
}

void AstarNavigation::SetState()
{
	this->localCostmap=NavigationMsgs::instance()->localCostmap;
	this->getPlan=NavigationMsgs::instance()->getPlan;
}

void AstarNavigation::GetState()
{
	localPath.header.seq++;
	localPath.poses=calc_final_path(ngoal_,closed_set);
	//NavigationMsgs::instance()->localPath.header.seq++;
	if (isDebug)
	{
		//localCostmap.Debug();
	}
	ClearMap();
}

void AstarNavigation::SpinOnce()
{
	gridRange=TF::CalcGridRange(localCostmap);
	nstart_ = {localCostmap.offset.x,localCostmap.offset.y,0.f,-1};
	//本当はここはGetPlanに置き換えたい
	ngoal_ = {localCostmap.offset.x+10,localCostmap.offset.y,0.f,-1};
	MakePlan();
	//for (size_t i = 0; i < rx_; i++)
	//{
	//	/* code */
	//}
	

}

bool AstarNavigation::verify_node(Node node){
	float px=TF::Index2Position(node.x,gridRange.l,localCostmap.info.resolution);
	float py=TF::Index2Position(node.y,gridRange.b,localCostmap.info.resolution);
	if (px<gridRange.l || gridRange.r<px) return false;
	if (py<gridRange.b || gridRange.t<py) return false;
	//障害物map上ならfalse
	int index=TF::Grid2Array(localCostmap.info.width,node.x,node.y);
	if(localCostmap.Data[index]>1)return false;
	//該当項目が全てなければOpenNodeに加える
	return true;
}



bool AstarNavigation::MakePlan() {
	open_set[calc_grid_index(nstart_)] = nstart_;

	while (1) {
		if (open_set.size() == 0) {
			std::cout << "openset is empty" << std::endl;
			break;
		}
		int c_id;
		//std::cout << "set" << open_set.size() << "\n";
		c_id = get_minimum_key(ngoal_);
		Node current = open_set[c_id];
		if (current.x == ngoal_.x && current.y == ngoal_.y) {
			cout << "find goal" << endl;
			ngoal_.pind = current.pind;
			ngoal_.cost = current.cost;
			break;
		}
		cout << "id" << c_id<<"x,y"<<current.x<<" "<<current.y;
		open_set.erase(c_id);
		//現在のコストが一番低いOpensetをClosedセットに加える
		closed_set[c_id] = current;
		Node node;
		for (uint i = 0; i < motion.size(); i++) {
			node = { (int) (current.x + motion[i][0]),
					 (int) (current.y + motion[i][1]),
					 (float) (current.cost + motion[i][2]),
					 (int) c_id
					  };

			int n_id = calc_grid_index(node);
			//if the node id not safe do nothing
			if (!verify_node(node))
				continue;
			//closed setにすでにある場合
			if (closed_set.find(n_id) != closed_set.end()) {
				//すでにClosedに設定されている場合
				continue;
			}
			//open_nodeに設定されていない場合
			if (open_set.find(n_id) != closed_set.end()) {
				//新しいノードの追加
				open_set[n_id] = node;
			}
			//closed nodeにも設定されておらず、open nodeにはすでに設定されている場合。
			else {
				if (open_set[n_id].cost > node.cost) {
					//このPathが現在最適なPathとなる
					open_set[n_id] = node;
				}
			}
		}
	}
	return true;
}
bool AstarNavigation::ClearMap(){
	open_set.clear();
	closed_set.clear();
	return true;
}

int AstarNavigation::get_minimum_key(Node &ngoal){
	cout<< "set"<<open_set.size()<<"\n";
	float min_cost=INFINITY;
	float min_cost_key;
	int key;
	int cost;
	for(auto itr =open_set.begin();itr!=open_set.end();itr++){
		key=itr->first;
		cost=open_set[key].cost+calc_heuristic(ngoal,open_set[key]); // @suppress("Invalid arguments") // @suppress("Field cannot be resolved")
		if(min_cost>cost){
			min_cost=cost;
			min_cost_key=key;
		}
	}
	return min_cost_key;
}

//Done
vector<PoseStamped> AstarNavigation::calc_final_path(Node ngoal, std::map<int,Node> closedset){
	vector<PoseStamped> poses={};
	PoseStamped pose={};
	pose.pose.position.x=TF::Index2Position(ngoal.x,gridRange.l,localCostmap.info.resolution);
	pose.pose.position.y=TF::Index2Position(ngoal.y,gridRange.b,localCostmap.info.resolution);
	poses.emplace_back(pose);
	int pind=ngoal.pind;
	while (pind!=-1){
		Node n=closed_set[pind];
		pose.pose.position.x=TF::Index2Position(n.x,gridRange.l,localCostmap.info.resolution);
		pose.pose.position.y=TF::Index2Position(n.y,gridRange.b,localCostmap.info.resolution);
		poses.emplace_back(pose);
		pind=n.pind;
	}
	return poses;
}


int AstarNavigation::calc_grid_index(Node node){
		return (node.y-gridRange.b)*localCostmap.info.width+(node.x-gridRange.l);
}

int AstarNavigation::calc_xyindex(float position, float min_pos){
	return (int)((position-min_pos)/localCostmap.info.resolution);
}







