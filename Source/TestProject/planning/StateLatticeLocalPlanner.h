// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "design/TemplateMethod.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "DWALocalPlanner.h"
#include <map>
#include <vector>
using std::map;
using std::vector;


class StateLatticeLocalPlanner : public TemplateMethod
{
	bool isDebug = true;
	OccupancyGrid localCostmap;
	Path localPath;
	Path globalPath;
	Geometry geometry;
	Vector3 localTarget;
	int minKey;
	float occupancyScale = 0.5f;
	float overrideScale = 0.5f;

	//==========EstimateVlue
	int selectedLane;
	Vector3 EvaluateOccupancyGrid();

	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;

public:
	StateLatticeLocalPlanner();
	~StateLatticeLocalPlanner();
};