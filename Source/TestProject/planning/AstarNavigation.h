// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "design/TemplateMethod.h"
#include "define/core_define.h"
#include "define/nav_define.h"
#include "define/geom_define.h"
#include <iostream>
#include <vector>
#include <map>
using std::cout;
using std::endl;

struct Node
{
	int x;
	int y;
	float cost;
	int pind;

	Node(int x = 0, int y = 0, float cost = 0.0f, int pind = 0)
	{
		this->x = x;
		this->y = y;
		this->cost = cost;
		this->pind = pind;
	}
	void print()
	{
		printf("x:%d,y:%d,cost:%f,pind:%d\n", x, y, cost, pind);
	}
};

//verified
static std::vector<std::vector<float>> get_motion_model()
{
	//if(debug) cout<<"got motion"<<endl;
	std::vector<std::vector<float>> motion = {
		{1, 0, 1}, 
		//{0, 1, 1},//真横は不可能
		{-1, 0, 1},
		//{0, -1, 1},//真横は不可能
		{-1, -1, sqrt(2.0f)},
		{-1, 1, sqrt(2.0f)},
		{1, -1, sqrt(2.0f)},
		{1, 1, sqrt(2.0f)}};
	return motion;
}



class AstarNavigation:public TemplateMethod{
private:
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;
	
	bool isDebug=true;
	OccupancyGrid localCostmap;
	GetPlan getPlan;
	Path localPath;
	Rect gridRange;
	bool debug=true;
	float rr_;

	static float calc_heuristic(Node &n1, Node &n2)
	{
		float w = 1.0f;
		float d = w * hypot(n1.x - n2.x, n1.y - n2.y);
		return d;
	}
	Vector2i start_position_;
	Vector2i goal_position_;
	Node nstart_;
	Node ngoal_;

	std::vector<std::vector<float>> motion;
	std::map<int,Node> open_set;
	std::map<int,Node> closed_set;
	int calc_xyindex(float position,float min_pos);
	float calc_grid_position(int index,float minp);
	int calc_grid_index(Node node);

public:

	AstarNavigation();
	bool SetStartNode();
	bool SetGoalNode();
	void MakePath(Vector2i position,float angle){}
	bool ClearMap();
	bool MakePlan();
	int get_minimum_key(Node &ngoal);
	vector<PoseStamped> calc_final_path(Node ngoal,std::map<int,Node> closedset);
	bool verify_node(Node node);

};
