// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "design/TemplateMethod.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include <map>
#include <vector>
using std::map;
using std::vector;

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RecoveryBehavior.generated.h"

class RecoveryBehavior : public TemplateMethod
{

	//===
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;

public:
	RecoveryBehavior();
	~RecoveryBehavior();
};

UCLASS()
class TESTPROJECT_API ARecoveryBehavior : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARecoveryBehavior();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
