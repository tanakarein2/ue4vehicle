// Fill out your copyright notice in the Description page of Project Settings.

#include "RecoveryBehavior.h"

RecoveryBehavior::RecoveryBehavior()
{
}

RecoveryBehavior::~RecoveryBehavior()
{
}

void RecoveryBehavior::SpinOnce(){

}
void RecoveryBehavior::GetState()
{
}

void RecoveryBehavior::SetState()
{
}

void RecoveryBehavior::Start()
{
}


// Sets default values
ARecoveryBehavior::ARecoveryBehavior()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ARecoveryBehavior::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARecoveryBehavior::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
