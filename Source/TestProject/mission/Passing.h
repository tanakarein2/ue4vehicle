// /*copy rigth 2018 Taiki Tanaka Under GPLlv3 License*/

#pragma once
#include "design/TemplateMethod.h"
#include "CoreMinimal.h"

/**
 * 
 */
class Passing:public TemplateMethod
{
	void SetState() override{};
	void SpinOnce() override;
	void GetState() override{};
	void Start() override{};


public:

	Passing();
	~Passing();
};
