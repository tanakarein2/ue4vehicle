// /*copy rigth 2018 Taiki Tanaka Under GPLlv3 License*/

#include "Passing.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "config/vehicle_config.h"
#include <iostream>
#include <math.h>
#include <vector>
using std::cout;
using std::endl;
using std::vector;
//=
#include "unreal/unreal_define.h"
#include "DrawDebugHelpers.h"
#include "Math/Color.h"

Passing::Passing()
{
    cout << "passing class" << endl;
}

Passing::~Passing()
{
    cout << "de pasing class" << endl;
}

void Passing::SpinOnce()
{
    float ttc = 1.f;
    float thw = 1.f; //time headway

    float nominalSpeed=0.f;
    float laneChangeDuration = 1.f;

    float dv =
        vehicle::CAR_LENGTH + safety::STANDSTILL_DISTANCE + thw * nominalSpeed;
    float x = 10.0f;
    float xtl = 40.f;
    float xcl = 50.f;
    float xlv = 0.f;

    float dGap = dv;

    float laneChangeDecisionTime = 0.f;
    float laneChangeStartTime = 5.f;
    float laneChangeEndTime = 10.f;

    float currentLeaderBound = xcl - dGap;
    float targetLeaderBound = xtl - dGap;
    float lagVehicleBound = xlv + dGap;

    float xclb = currentLeaderBound;
    float xtlb = targetLeaderBound;
    float xlb = lagVehicleBound;

    float move = 30.f;
    float xt = x + move;
    bool travelAtNominalSpeed = true;

    //safety at current lane
    bool isCL = true;
    bool isTL = true;
    bool isLV = true;
    bool isNorminal = false;
    //map<string, float> x;
    /*
    xmin["start"]=min(xtlb,xclb);
    xlb=xlb["start"]
    //case1
    if(x["start"]>xmin["start"]){
        //speedDown;
    }
    //case 2
    else if(x["start"]<xlb["start"] && x["start"]<xmin["start"]){
        //SpeedUp
    }
    //case 3
    else if(x["start"]<xlb["start"]&&x["start"]=xmin["start"]){
        //follow closest
    }
    //case 4
    else if(xmin["start"]>x["start"] && x["start"]>=xlb["start"] && !isNominal){
        //speed up
    }
    //case 5
    else if(xmin["start"]>x["start"] && x["start"]>=xlb["start"] && isNominal){
        //lanechange at nominal Speed;
    }
    //case 6
    else if(xmin["start"]=x["start"]&&x["start"]>=xlb["start"]&& !isNominal){
        //follow tl/cl
    }
    //case 7
    else if(xmin["start"]=x["start"]&&x["start"]>=xlb["start"]&&isNominal){
        //lane change at nominal speed
    }
    */
    if (currentLeaderBound < xt)
    {
        cout << "current leader safe" << endl;
    }
    else if (0)
    {
        cout << "wait for enough gap" << endl;
    }
    //
}