// /*copy rigth 2018 Taiki Tanaka Under GPLlv3 License*/

#include "Careful.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "msgs/sensor_msgs.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "config/vehicle_config.h"
#include "config/control_config.h"
#include <iostream>
#include <math.h>
#include <vector>
using std::cout;
using std::endl;
using std::vector;
//=
#include "unreal/unreal_define.h"
#include "DrawDebugHelpers.h"
#include "Math/Color.h"

Careful::Careful()
{
    cout << "Careful class" << endl;
}

Careful::~Careful()
{
    cout << "del Careful class" << endl;
}

float Careful::Follow(float dist)
{
    float gain = 4.f;
    float control = gain * (dist - safetyDistance);
    distOld = dist;
    return control;
}
void Careful::SetState()
{
    //this->controlCommand = VehicleCmdMsgs::instance()->controlCommand;
    //this->vehicleState = VehicleStateMsgs::instance()->vehicleState;
}
void Careful::SpinOnce()
{
    float dist = ultraSonic.dist;

    // float thw=dist/ackermannDrive.speed;
    // float ttc=dist/(ackermannDrive.speed-otherVelo);
    const float StopDistCoeff = 0.5f;
    safetyDistance = vehicle::CAR_LENGTH + safety::STANDSTILL_DISTANCE + StopDistCoeff * ackermannDrive.speed;
    controlCommand.linear_velocity = ctrl::target_speed;
    if (dist < safetyDistance)
    {
        float val = Follow(dist);
        controlCommand.linear_velocity += val;
    }
    else
    {
        controlCommand.linear_velocity = ctrl::target_speed;
    }
}
void Careful::GetState()
{
    //VehicleCmdMsgs::instance()->controlCommand = this->controlCommand;
    if (isDebug)
    {
        Print::Start("Careful");
        controlCommand.Debug();
        Print::End("Careful");

    }
}