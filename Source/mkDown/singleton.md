##singleton実装のBestPractice
すべてのクラスに共通した変数を作りたい

singletonのインスタンスは一つしか存在しない。.

    static変数の初期化順序が未定義であるために、予期せぬ値を参照してしまう。
    複数のスレッドから同時に初期化処理を行なった際、適切な同期を行なえない。
    シングルトンクラスのコンストラクタでシングルトンを参照した際、無限ループが発生する。

>>>>
#include <iostream>

class Pos{
public:
	float x=2;
	float y;
};


class Singleton
{
private:

    Singleton();
    ~Singleton();

public:
    Pos pos;
    int a=0;
    static Singleton& instance()
    {
        static Singleton INSTANCE;
        return INSTANCE;
    }

    void Test();
};

void Singleton::Test()
{
    std::cout << "Test() called" << std::endl;
}

Singleton::Singleton()
{
    std::cout << "CONSTRUCTOR CALLED" << std::endl;
}

Singleton::~Singleton()
{
    std::cout << "DESTRUCTOR CALLED" << std::endl;
}

void MyFunction()
{
    // use the singleton class
    Singleton * MySingleton = &Singleton::instance();
    MySingleton->Test();
    std::cout<<"a"<<Singleton::instance().a<<std::endl;
    Singleton::instance().pos.x;
}


int main()
{
    MyFunction();
    return 0;
}

>>>
ここまで