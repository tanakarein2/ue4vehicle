逆引きiterator（イテレータ）

    int 型の要素を持つ std::vector へのイテレータを宣言する方法は？

        std::vector<int>::iterator itr;

    int 型の要素を持つ std::vector への const なイテレータを宣言する方法は？

        std::vector<int>::const_iterator itr;

    int 型の要素を持つ std::vector v の先頭を指すイテレータを宣言する方法は？

        std::vector<int>::iterator itr = v.begin();

    または

        auto itr = v.begin();        // 推奨

    int 型の要素を持つ std::vector v の末尾（最後の要素の次）を指すイテレータは？

        v.end();        //  型は vector<int>::iterator

    int 型の要素を持つ std::vector v の先頭を指す const なイテレータを宣言する方法は？

        std::vector<int>::const_iterator itr = v.cbegin();

    または

        auto itr = v.cbegin();      // 推奨

    int 型の要素を持つ std::vector v の末尾（最後の要素の次）を指す const なイテレータは？

        v.cend();           // 型は vector<int>::const_iterator

    イテレータ itr をひとつ進める方法は？

        ++itr;

    イテレータ itr をひとつ前に戻す方法は？

        --itr;

    ランダム・アクセス可能なイテレータ itr を n 進める方法は？

        itr += n;

    ランダム・アクセス可能なイテレータ itr を n 前に戻す方法は？

        itr -= n;

    first と last 間の要素数を求める方法は？

        std::distance(first, last);

    イテレータ itr の指す先の要素を参照する方法は？

        *itr

    イテレータ itr の指す先の要素に val を代入する方法は？

        *itr = val;

    イテレータを使って vector<int> v の要素を最初から最後まで参照する方法は？

        for(auto itr = v.begin(); itr != v.end(); ++itr) {
            *itr;
        }

    イテレータを使って vector<int> v の要素を最後から最初まで参照する方法は？

        for(auto itr = v.rbegin(); itr != v.rend(); ++itr) {
            *itr;
        }

    範囲 [first, last) を全て処理する方法は？

        while( first != last ) {
            何らかの処理;
            ++first;
        }

    ランダム・アクセス可能なイテレータ first, last を使って、範囲 [first, last) の要素をソートする方法は？

        std::sort(first, last);

    イテレータ first, last を使って、範囲 [first, last) の要素の合計を計算する方法は？

        std::accumulate(first, last, 0);

    イテレータ first, last を使って、範囲 [first, last) の要素順序を反転させる方法は？

        std::reverse(first, last);

    イテレータ first, last を使って、範囲 [first, last) の要素値が val の個数を数える方法は？

        std::count(first, last, val);


最も早い
	vector<int> intvector =
	{ 2, 3, 4 };
	for (int i : intvector)
	{
		int a = i;
	}