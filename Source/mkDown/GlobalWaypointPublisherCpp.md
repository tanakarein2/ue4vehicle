// Fill out your copyright notice in the Description page of Project Settings.

#include "GlobalWaypointPublisher.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/waypoint_define.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include "config/waypoint_config.h"
#include <iostream>
#include <vector>
#include <math.h>
#include <map>
using std::cout;
using std::endl;
using std::map;
using std::vector;

#include "define/unreal_define.h"
#include "DrawDebugHelpers.h"
#include "Math/Color.h"

WaypointPublisher::WaypointPublisher()
{
	cout << "create waypoint publisher" << endl;
}

WaypointPublisher::~WaypointPublisher()
{
	cout << "del waypoint publisher" << endl;
}


Point WaypointPublisher::MakeStraight(vector<Point> &waypoints, float x, float y, float d, float len, float rad)
{
	float tmpx = 0.f;
	float tmpy = 0.f;
	float tmpth = 0.f;
	float tmpx_old = 0.f;
	float tmpy_old = 0.f;
	//Point tmp = {0.f, 0.f, 0.f};
	int num = int(len / d);
	for (int i = 0; i < num; i++)
	{

		tmpx = x + d * cos(rad) * i;
		tmpy = y + d * sin(rad) * i;
		tmpth = atan2(tmpx - tmpx_old, tmpy - tmpy_old);
		tmpx_old = tmpx;
		tmpy_old = tmpy;
		//global waypoints
		waypoints.emplace_back(Point(tmpx, tmpy, 0.f));
	}
	return Point(tmpx, tmpy, 0.f);
}

Point WaypointPublisher::MakeCurve(vector<Point> &waypoints, float x, float y, float d, float r, float rad, int x_dir, int y_dir)
{
	float tmpx = 0.f;
	float tmpy = 0.f;
	int num = rad / abs(d);
	for (int i = 0; i < num; i++)
	{
		tmpx = x + 2.f * x_dir * r * i / num;
		tmpy = y + y_dir * r * sin(d * i);
		//global waypoints
		waypoints.emplace_back(Point(tmpx, tmpy, 0.f));
		//UE_LOG(LogTemp, Warning, TEXT("x,y,z <%.4f, %.4f, %.4f>"),tmpx,tmpy,0.f);
	}
	return Point(tmpx, tmpy, 0.f);
}

vector<Point> WaypointPublisher::RightLane()
{
	vector<Point> waypoints = {};
	Point tmp = {-3.0f, -60.f, offsetz};
	tmp = MakeStraight(waypoints, tmp.x, tmp.y + 0.f, 1.0f, 120.0f, M_PI_2);
	tmp = MakeCurve(waypoints, tmp.x + 0.f, tmp.y + 0.f, 0.05f, 29.f, M_PI, 1, 1);
	tmp = MakeStraight(waypoints, tmp.x + 0.f, tmp.y + 0.f, 1.0f, 120.0f, -M_PI_2);
	tmp = MakeCurve(waypoints, tmp.x + 0.f, tmp.y + 0.f, 0.05f, 29.f, M_PI, -1, -1);
	return waypoints;
}
vector<Point> WaypointPublisher::LeftLane()
{
	vector<Point> waypoints = {};
	Point tmp = {-9.0f, -60.f, offsetz};
	tmp = MakeStraight(waypoints, tmp.x, tmp.y + 0.f, 1.0f, 120.0f, M_PI_2);
	tmp = MakeCurve(waypoints, tmp.x + 0.f, tmp.y + 0.f, 0.05f, 35.f, M_PI, 1, 1);
	tmp = MakeStraight(waypoints, tmp.x + 0.f, tmp.y + 0.f, 1.0f, 120.0f, -M_PI_2);
	tmp = MakeCurve(waypoints, tmp.x + 0.f, tmp.y + 0.f, 0.05f, 35.f, M_PI, -1, -1);
	return waypoints;
}

void WaypointPublisher::SetState() {}
void WaypointPublisher::GetState() {}
void WaypointPublisher::SpinOnce() {}
void WaypointPublisher::Start()
{
	LaneArrayMsgs::instance().lane_array_.id = 0;
	LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints= RightLane();
	LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::LEFT].waypoints = LeftLane();
	LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::RIGHT].header = {1, 0, "lanes[right] array", 0.f};
}
//=======================================================================================================

// Called when the game starts or when spawned
void AWaypointPublisher::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWaypointPublisher::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bInitialized)
	{
		way->Update();
		DrawWaypointsTick(LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints);
		DrawWaypointsTick(LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::LEFT].waypoints);
		bInitialized = true;
	}
}

// Sets default values
AWaypointPublisher::AWaypointPublisher()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AWaypointPublisher::DrawWaypointsTick(const vector<Point> &positions)
{
	UnrealDrawer::DrawPoints(GetWorld(),positions,FColor::Blue,1.5f,3.f,1000.f);
}

AWaypointPublisher::~AWaypointPublisher()
{
	cout
		<< "del Awaypoint class";
}
