GlobalPlanner::GlobalPlanner()
{
	cout << "create Global Planner Class \n";
}

GlobalPlanner::~GlobalPlanner()
{
	cout << "del Global Planner Class \n";
}

// get stare pos from pos speed angle
void GlobalPlanner::LookAheadModel()
{
	look_forward_distance = look_ahead + look_speed_gain * hypotf(geometry.twist.linear.x, geometry.twist.linear.y);
	stare_pos.x = geometry.pose.position.x + look_forward_distance * cos((double)geometry.pose.euler.z);
	stare_pos.y = geometry.pose.position.y + look_forward_distance * sin((double)geometry.pose.euler.z);
	if (isDebug)
	{
		cout << "=======GlobalPlanner======" << endl;
		cout << "posx" << geometry.pose.position.x << "posY" << geometry.pose.position.y << endl;
		cout << "gear" << vehicleState.gear << "lookahead " << look_forward_distance << endl;
		cout << "sx" << stare_pos.x << "sy " << stare_pos.y << endl;
		cout << "=======GlobalPlanner======" << endl;
	}
	return;
}

// get target waypoint from waypoints
void GlobalPlanner::SearchNearestWaypoints(vector<Point> &waypoints, float x, float y)
{
	float min_distance = 10000.f;
	search.min = search.positionIndex - search.range;
	search.max = search.positionIndex + search.range;
	if (search.min < 0)
	{
		search.min = 0;
		search.max = waypoints.size();
	}
	if (search.max > waypoints.size())
	{
		search.min = 0;
		search.max = waypoints.size();
	}
	float tmpx = x;
	float tmpy = y;

	for (int i = search.min; i < search.max; i++)
	{
		double delx = tmpx - waypoints[i].x;
		double dely = tmpy - waypoints[i].y;
		double dist = sqrt(pow(delx, 2) + pow(dely, 2));
		if (min_distance > dist)
		{
			min_distance = dist;
			search.positionIndex = i;
			//std::cout<<"i:"<<i<<std::endl;
		}
	}

	waypoint_target.x = waypoints[search.positionIndex].x;
	waypoint_target.y = waypoints[search.positionIndex].y;
	//cout<<"dist"<<min_distance<<endl;
	//std::cout<<"mm"<<position.y<<"y"<<waypoints[positionIndex].y<<"id:"<<positionIndex<<std::endl;

	return;
}

//================================
void GlobalPlanner::Start()
{
	this->laneIndex = lane_config::lane_index::LEFT;
	this->newLaneIndex = this->laneIndex;
	this->lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints = LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints;
	globalPath.header = {0, 0, "GlobalPath", 0.f};
}

void GlobalPlanner::SetState()
{
	this->lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints = LaneArrayMsgs::instance().lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints;
	this->geometry = VehicleStateMsgs::instance().geometry;
	this->vehicleState = VehicleStateMsgs::instance().vehicleState;
}

void GlobalPlanner::GetState()
{

	globalPath.target = this->waypoint_target;
	//globalPath.waypoints.assign(&lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints[search.min], &lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints[search.max]);
	globalPath.header.seq++;
	//NavigationMsgs::instance().globalPath = this->globalPath;
	if (isDebug)
	{
		NavigationMsgs::instance().globalPath.Debug();
	}
}

void GlobalPlanner::SpinOnce()
{
	LookAheadModel();
	//最近棒点探索
	SearchNearestWaypoints(
		this->lane_array_.lanes[lane_config::lane_index::RIGHT].waypoints,
		stare_pos.x,
		stare_pos.y);
}



class GlobalPlanner : public TemplateMethod
{
	struct Search
{
	int positionIndex = 0;
	int range = 50;
	int min = 0;
	int max = 0;
};

	Search search;
	bool isDebug = true;
	int laneIndex;
	int newLaneIndex;
	//look forward offset
	float look_ahead = 1.5f;
	float look_speed_gain = 1.5f;
	float look_forward_distance;

	Point waypoint_target = {};
	Point stare_pos;
	Geometry geometry;
	VehicleState vehicleState;
	void SearchNearestWaypoints(vector<Point> &waypoints, float x, float y);
	void LookAheadModel();

	//===
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;
	//===
public:
	GlobalPlanner();
	~GlobalPlanner();
	LaneArray_ lane_array_;
	LaneArray lane_array;

	Path_ globalPath = {};
	//map<int, vector<Point>> localWaypointsList;
};
