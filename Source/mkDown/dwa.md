class NumericalExtension
{
public:
static float AngleRangeCorrector(float angle)
{
if (angle > M_PI)
{
while (angle > M_PI)
{
angle -= 2 _ M_PI;
}
}
else if (angle < -M_PI)
{
while (angle < -M_PI)
{
angle += 2 _ M_PI;
}
}
return angle;
}

    static ObjPose calc_nearest_obs(const Pose &pose, ObjPose &allObj, float area_dis2obs = 5)
    {
    	ObjPose nearObj;
    	for (int i = 0; i < allObj.obj.poses.size(); i++)
    	{
    		float dis2obs = hypot(
    			pose.position.x - allObj.obj.poses[i].position.x,
    			pose.position.y - allObj.obj.poses[i].position.y);
    		if (dis2obs < area_dis2obs)
    		{
    			nearObj.obj.poses.push_back(allObj.obj.poses[i].position);
    		}
    	}
    	return nearObj;
    }

    static float score_heading_angle(Pose &pose, GetPlan &getPlan, int size = 10)
    {

    	float angle_to_goal = atan2(getPlan.goal.position.y - pose.position.y, getPlan.goal.position.x - pose.position.x);
    	float score_angle = angle_to_goal - pose.euler.z;
    	score_angle = abs(NumericalExtension::AngleRangeCorrector(score_angle));
    	score_angle = M_PI - score_angle;
    	return score_angle;
    }

    static float score_heading_velo(float velocity)
    {
    	return velocity;
    }

    static float score_obstcle(Locus &locus, ObjPose &nearObj)
    {
    	float score_obstacle = 2;
    	float temp_dis_to_obs = 0.0;
    	for (int j = 0; j < locus.size(); j++)
    	{ //for i in range(len(locus.x)):
    		for (int i = 0; i < nearObj.obj.poses.size(); i++)
    		{
    			//for obs in nearest_obs:
    			temp_dis_to_obs = hypot(
    				locus.poses[j].position.x - nearObj.obj.poses[i].position.x,
    				+locus.poses[j].position.y - nearObj.obj.poses[i].position.y);
    			//最大スコアよりも小さいスコアならば入れ替え
    			if (temp_dis_to_obs < score_obstacle)
    			{
    				score_obstacle = temp_dis_to_obs;
    			}
    			//ここは改善の余地あり
    			//ぶつかったら即終了
    			if (temp_dis_to_obs < 1.f)
    			{
    				score_obstacle = -1;
    				return score_obstacle;
    			}
    		}
    	}
    	return score_obstacle;
    }

};
