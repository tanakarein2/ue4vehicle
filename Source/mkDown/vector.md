## assign

assign(first, last) により[first, last) の範囲のデータを vector に代入することができる。

以下は、配列のデータの一部を vector に代入する例だ。

    int ar[] = {1, 2, 3, 4, 5, 6, 7};
    vector<int> v{9, 8};
    v.assign(&ar[0], &ar[3]);        // v の内容は {1, 2, 3} になる





    for (auto lane : {Way::Lane::left, Waypoint::middle, Waypoint::right})
    {
    	//Point tmp = Waypoint::globalWaypointsList[lane][Waypoint::index];

    	float V = 0.f; //Vo+Vth+Vf;
    }
    return newLane;
