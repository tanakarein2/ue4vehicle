## CPP

''' .cpp
TestClass::TestClass(){

}

TestClass::~TestClass(){

}

void TestClass::GetState()
{
}

void TestClass::SetState()
{
}

void TestClass::Start()
{
}

void TestClass::SpinOnce()
{
}

## Header

#pragma once

#pragma once
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "design/TemplateMethod.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SensorFusion.generated.h"

class TestClass : public TemplateMethod
{

bool isDebug = true;
void SetState() override;
void SpinOnce() override;
void GetState() override;
void Start() override;

public:
TestClass();
~TestClass();
};

'''
