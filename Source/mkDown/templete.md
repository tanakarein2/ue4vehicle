
### tempelte highlight

❜❜❜

void OccupancyGrid::UpdateGrid()
{
	//限定されたオブジェクトにたいして
	for (int i = 0; i < objectsLocal.size(); i++)
	{
		float xg = objectsLocal[i].x;
		float yg = objectsLocal[i].y;

		Vector2<float> xyg = {objectsLocal[i].x,
							  objectsLocal[i].y};
		//グリッドの位置は整数のためIntにキャスト
		Vector2<int> ankerxy = {localMap.centerX, localMap.centerY};
		Vector2<int> xyi = CoordinateTransform::WorldGrid2Local<float>(xyg, ankerxy, resolution);

		int ix = int(xg / resolution + localMap.centerX);
		int iy = int(yg / resolution + localMap.centerY);
		int localGridX;
		int localGridY;
		CoordinateTransform::WorldGrid2Local(localGridX, localGridY, localMap.centerX, localMap.centerY, xg, yg);
		
		//indexに直す
		int index = W * iy + ix;
		Data[index] = 1;

		//GaussianLikeHoodUpdate();
	}
	Data[W * localMap.centerY + localMap.centerX] = 9;
}



	//float or double
	template <typename T>
	static Vector2<int> LocalGrid2Word(const Vector2<int> localGrid, const Vector2<int> anker, const T resolution)
	{
		Vector2<int> worldGrid = {
			int((localGrid.x - anker.x) / resolution),
			int((localGrid.y - anker.y) / resolution)};
		return worldGrid;
	}

	//float or double
	template <typename T>
	static Vector2<int> WorldGrid2Local(const Vector2<T> worldGrid, const Vector2<int> anker, const T resolution)
	{
		Vector2<int> localGrid = {
			int((worldGrid.x / resolution) + anker.x),
			int((worldGrid.y / resolution) + anker.y)};
		return localGrid;
	}

❜❜❜
