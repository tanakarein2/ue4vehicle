// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#define _USE_MATH_DEFINES
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"

#include "design/TemplateMethod.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GlobalWaypointPublisher.generated.h"

class WaypointPublisher : public TemplateMethod
{
private:
	//vector<Point> waypoints = {};

	//update state on every tick
	FVector position;
	Vector3 euler;
	FVector velocity;
	float offsetz = 1.1f;

	Point MakeStraight(vector<Point> &waypoints, float x, float y, float d, float len, float rad);
	Point MakeCurve(vector<Point> &waypoints, float x, float y, float d, float r, float rad, int x_dir, int y_dir);
	vector<Point> LeftLane();
	vector<Point> RightLane();
	void OverRide(float x, float y);

	//===
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;
	//===
public:
	WaypointPublisher();
	~WaypointPublisher();
};

UCLASS()
class TESTPROJECT_API AWaypointPublisher : public AActor
{
	GENERATED_BODY()
	TemplateMethod *way = new WaypointPublisher();
	bool bInitialized = false;
	void DebugDraw();
	void DrawWave(float time);
	void DrawWaypoints(const vector<Point> &position);
	void DrawWaypointsTick(const vector<Point> &positions);
	void DrawWaypointTargetTick(const Point &position);

	//custom

public:
	// Sets default values for this actor's properties
	AWaypointPublisher();
	~AWaypointPublisher();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
