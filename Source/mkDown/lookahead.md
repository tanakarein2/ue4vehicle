// get stare pos from pos speed angle
Point WaypointSearcher::LookAheadModel(Pose pose) {
    //look forward offset
    float look_ahead = 1.5f;
    float look_speed_gain = 1.5f;
    float look_forward_distance;
    Point starePoint = {};
    look_forward_distance = look_ahead + look_speed_gain * ackermannDrive.speed;
    starePoint.x = pose.position.x + look_forward_distance * cos((double) pose.euler.z);
    starePoint.y = pose.position.y + look_forward_distance * sin((double) pose.euler.z);
    return starePoint;
}



// get target waypoint from waypoints
Point WaypointSearcher::SearchNearestWaypoints(const vector<Waypoint> waypoints, Point starePoint) {
    float min_distance = 10000.f;
    search.min = search.positionIndex - search.range;
    search.max = search.positionIndex + search.range;
    if (search.min < 0) {
        search.min = 0;
        search.max = waypoints.size();
    }
    if (search.max > waypoints.size()) {
        search.min = 0;
        search.max = waypoints.size();
    }

    for (int i = search.min; i < search.max; i++) {
        double delx = starePoint.x - waypoints[i].pose.pose.position.x;
        double dely = starePoint.y - waypoints[i].pose.pose.position.y;
        double dist = sqrt(pow(delx, 2) + pow(dely, 2));
        bool isLower = chmin<float>(min_distance, dist);
        if (isLower) {
            search.positionIndex = i;
            //std::cout<<"i:"<<i<<std::endl;
        }
    }
    Point target = {
            waypoints[search.positionIndex].pose.pose.position.x,
            waypoints[search.positionIndex].pose.pose.position.y,
            0.f};
    return target;
}