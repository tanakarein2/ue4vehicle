#H

#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include <map>
#include <vector>
using std::map;
using std::vector;

class LocalPlanner : public TemplateMethod
{
	bool isDebug = true;
	OccupancyGrid localCostmap;
	Path localPath;
	Path globalPath;
	Geometry geometry;
	Point minimumCost;
	Point localTarget;
	int minKey;
	float occupancyScale = 2.f;
	float overrideScale = 0.1f;

	//==========EstimateVlue
	int selectedLane;
	Point EvaluateOccupancyGrid();

	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;

public:
	LocalPlanner();
	~LocalPlanner();
};

# CPP
LocalPlanner::LocalPlanner()
{
	cout << "create Local Planner Class" << endl;
}
LocalPlanner::~LocalPlanner()
{
	cout << "del Local Planner Class" << endl;
}
void LocalPlanner::Start()
{
	localPath.header = {0, time(NULL), "local path", 0.f};
}

void LocalPlanner::SetState()
{
	this->localCostmap = NavigationMsgs::instance().localCostmap;
	this->globalPath=NavigationMsgs::instance().globalPath;
	this->geometry = VehicleStateMsgs::instance().geometry;
}

void LocalPlanner::GetState()
{
	NavigationMsgs::instance().localPath = localPath;
	if (isDebug)
	{
		NavigationMsgs::instance().localPath.Debug();
	}
}

void LocalPlanner::SpinOnce()
{
	Point NavigationGoal = EvaluateOccupancyGrid();
	/*ここでLocalとGlobalの使い分け
		条件は２つ
			1.ぶつかるかどうかー＞OccupancyGridにより判定->Plan変更
			2.waypointから離れすぎているかどうか->RecoveryMotionへ遷移
		*/
	/// wrench method選択
	localPath.target = this->minimumCost;
	localPath.header.seq++;
}

Point LocalPlanner::EvaluateOccupancyGrid()
{
	int stepT = 10; //steps of scale
	int stepX = 2;
	int stepY = 2;
	float minCost = 10.f;
	float currentCost = 0.f;
	int keyX = 0;
	int minKeyX = 0;
	this->minimumCost.x = 0;
	//
	for (int x = 0; x < localCostmap.info.width; x += stepX)
	{
		keyX++;
		currentCost = 0.f;

		for (int y = localCostmap.occProp.offset.y; y < localCostmap.info.height; y += stepY)
		{
			//Indexがある限りはｘｙはGrid上の値
			int localCostmapIndex = IndexTransform::Grid2Array(localCostmap.info.width, x, y);
			currentCost += occupancyScale * float(localCostmap.Data[localCostmapIndex]);
		}
		float overrideCost = x - localCostmap.occProp.offset.x;
		currentCost += overrideScale * abs(overrideCost);
		if (minCost > currentCost)
		{
			minCost = currentCost;
			minKeyX = x;
		}
		if (isDebug)
		{
			//cout << "key" << x << "cost" << currentCost << "\n";
		}
	}
	float tmpx;
	float tmpy;
	//格子上の点がどこに移動するか
	CoordinateTransform::Grid2Position(
		tmpx, tmpy,
		localCostmap.occProp.offset.x, localCostmap.occProp.offset.y,
		minKeyX, localCostmap.info.height, localCostmap.info.resolution);
	float localCostmapPosX = tmpx;
	float localCostmapPosY = tmpy;
	//Localでの格子上の点はワールドでどこに位置するのか
	CoordinateTransform::Local2World(tmpx, tmpy, geometry.pose.euler.z, geometry.pose.position.x, geometry.pose.position.y);
	this->minimumCost = {tmpx, tmpy, minCost};

	return this->minimumCost;
}
