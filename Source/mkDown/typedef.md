irst off, the way that struct is declared is in the style of C. In C++ you should just do:

struct foo
{
bool my_bool;
int my_int;
};

In both C and C++, initialization is a separate step from allocation. If you always want to initialize the members of your struct, use default initialization syntax like this:

struct foo
{
bool my_bool{};
bool my_int{};
};

In older versions of C++ you need to manually write a default constructor that initializes all the members (the newer syntax above is just sugar for this):

struct foo
{
foo() : my_bool(), my_int() { }
bool my_bool;
int my_int;
};

As @sbi notes, if you want to manually initialize the struct, even without the default constructor, you can do foo myFoo = foo();

class VehicleState
{
VehicleState(){};
VehicleState(VehicleState const &);

public:
staic VehicleState &instance()
{
staic VehicleState Instance;
return Instance;
}
Point point;
Point velocity;
float angle;
};

    static Vector2 World2Local(float worldX, float worldY, float th, float tx, float ty)　
    {
    	float tmpx = worldX - tx;
    	float tmpy = worldY - ty;
    	float tmpth = -th + M_PI_2;
    	//printf("ox%lf,oy%lf \n",x,y);

    	//計算途中に値が入れ替わることを防ぐために一時変数をおいた
    	float x = cosf(tmpth) * tmpx - sinf(tmpth) * tmpy;
    	float y = sinf(tmpth) * tmpx + cosf(tmpth) * tmpy;
    	Vector2 localPos = {x, y};
    	//printf("ox%lf,oy%lf \n",x,y);
    	return localPos;
    }

//templete <typename T>
class CoordinateTransform
{
public:

    static void World2Local(float &x, float &y, float th, float tx, float ty)
    {
    	float tmpx = x - tx;
    	float tmpy = y - ty;
    	float tmpth = -th + M_PI_2;
    	//printf("ox%lf,oy%lf \n",x,y);

    	//計算途中に値が入れ替わることを防ぐために一時変数をおいた
    	x = cosf(tmpth) * tmpx - sinf(tmpth) * tmpy;
    	y = sinf(tmpth) * tmpx + cosf(tmpth) * tmpy;
    	//printf("ox%lf,oy%lf \n",x,y);
    	return;
    }

    static void Local2World(float &x, float &y, float th, float tx, float ty)
    {
    	float tmpx = x;
    	float tmpy = y;
    	float tmpth = th - M_PI_2;
    	//計算途中に値が入れ替わることを防ぐために一時変数をおいた
    	x = cosf(tmpth) * tmpx - sinf(tmpth) * tmpy + tx;
    	y = sinf(tmpth) * tmpx + cosf(tmpth) * tmpy + ty;
    	return;
    }

    static void Position2Grid(int &gridPositionX, int &gridPositionY,
    						  int localMap.centerX, int localMap.centerY,
    						  float worldPositionX, float worldPositionY,
    						  float resolution)
    {
    	gridPositionX = int(worldPositionX / resolution + localMap.centerX);
    	gridPositionY = int(worldPositionY / resolution + localMap.centerY);
    	return;
    }

    static void Grid2Position(float &worldPositionX, float &worldPositionY,
    						  float localMap.centerX, float localMap.centerY,
    						  int gridPositionX, int gridPositionY,
    						  float resolution)
    {

    	worldPositionX = float((gridPositionX - localMap.centerX) * resolution);
    	worldPositionY = float((gridPositionY - localMap.centerY) * resolution);
    	return;
    }

};
