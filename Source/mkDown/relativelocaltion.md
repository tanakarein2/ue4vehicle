To transform from world location to relative location use InverseTransformPosition:
Code:

FVector relativeLoc = GetTransform().InverseTransformPosition(myComponent->GetComponentLocation());

To transform from relative location to world location use TransformPosition:
Code:

FVector someRelativeLoc;
FVector worldLoc = GetTransform().TransformPosition(someRelativeLoc);

    /*
    	CoordinateTransform::Local2World(tmpx, tmpy, geometry.pose.euler.z, geometry.pose.position.x, geometry.pose.position.y);
            static void Local2World(float &x, float &y, float th, float tx, float ty)
    {
        float tmpx = x;
        float tmpy = y;
        float tmpth = th - M_PI_2;
        //計算途中に値が入れ替わることを防ぐために一時変数をおいた
        x = cosf(tmpth) * tmpx - sinf(tmpth) * tmpy + tx;
        y = sinf(tmpth) * tmpx + cosf(tmpth) * tmpy + ty;
        return;
    }
    */


        /*

        static void World2Local(float &x, float &y, float th, float tx, float ty)
    {
        float tmpx = x - tx;
        float tmpy = y - ty;
        float tmpth = -th + M_PI_2;
        //printf("ox%lf,oy%lf \n",x,y);

        //計算途中に値が入れ替わることを防ぐために一時変数をおいた
        x = cosf(tmpth) * tmpx - sinf(tmpth) * tmpy;
        y = sinf(tmpth) * tmpx + cosf(tmpth) * tmpy;
        //printf("ox%lf,oy%lf \n",x,y);
        return;
    }
    */



4

If r(t)=x(t)2+y(t)2−−−−−−−−−−√
, then
r˙=xx˙+yy˙x2+y2−−−−−−√

If θ(t)=arctan[y(t)/x(t)]
, then
θ˙=xy˙−x˙yx2+y2

Alternatively:

If x(t)=r(t)cos[θ(t)]
, then
x˙=r˙cosθ−rθ˙sinθ

If y(t)=r(t)sin[θ(t)]
, then
y˙=r˙sinθ+rθ˙cosθ