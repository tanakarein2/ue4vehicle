http://wiki.ros.org/mvsim

sudo apt-get install ros-melodic-mvsim

apt-get install libmrpt-dev

roslaunch mvsim mvsim_demo_2robots.launch



/clicked_point
/clock
/initialpose
/move_base_simple/goal
/mvsim_simulator/parameter_descriptions
/mvsim_simulator/parameter_updates
/r1/amcl_pose
/r1/base_pose_ground_truth
/r1/chassis_markers
/r1/chassis_polygon
/r1/cmd_vel
/r1/laser1
/r1/odom
/r1/particlecloud
/r2/amcl_pose
/r2/base_pose_ground_truth
/r2/chassis_markers
/r2/chassis_polygon
/r2/cmd_vel
/r2/laser1
/r2/odom
/r2/particlecloud
/rosout
/rosout_agg
/simul_map
/simul_map_metadata
/simul_map_updates
/statistics
/tf
/tf_static


tf
transforms: 
  - 
    header: 
      seq: 0
      stamp: 
        secs: 428
        nsecs: 650000000
      frame_id: "/r2/odom"
    child_frame_id: "/r2/base_link"
    transform: 
      translation: 
        x: -10.0
        y: 0.0
        z: 0.0
      rotation: 
        x: 0.0
        y: 0.0
        z: 0.0
        w: 1.0
