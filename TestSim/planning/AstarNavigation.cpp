// Fill out your copyright notice in the Description page of Project Settings.
//============================================================================
// Name        : astar.cpp
// Author      : taiki tanaka
// Version     : v1
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "AstarNavigation.h"
#include "define/tf_define.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <map>


AstarNavigation::AstarNavigation(float grid_size, float robot_radius){
	//xは  Tx=W/2のため左右対称
	grid_range.r=(W-Tx)*reso_;
	grid_range.l=-(Tx-1)*reso_;
	//y方向は多少のオフセットTyを用いているため位置がずれる
	grid_range.f=(H-Ty)*reso_;
	grid_range.b=-(Ty-1)*reso_;
	reso_=grid_size;
	rr_=robot_radius;
    motion=get_motion_model();
}

float AstarNavigation::calc_grid_position(int index, float minp){
	float pos=index*reso_+minp;
	return pos;
}
int AstarNavigation::calc_grid_index(Node node){
		return (node.y-miny_)*W+(node.x-minx_);
}

bool AstarNavigation::SetStartNode(Vector2i &start_position) {
	this->start_position_ = start_position;
	nstart_ = {
			calc_xyindex(start_position_.x, minx_),
		    calc_xyindex(start_position_.y, miny_),
			0.f,
			-1
	};
	return true;
}
int AstarNavigation::calc_xyindex(float position, float min_pos){
	return (int)((position-min_pos)/reso_);
}

bool AstarNavigation::SetGoalNode(Vector2i &goal_position) {
	this->goal_position_ = goal_position;
	ngoal_ = {
			calc_xyindex(goal_position.x, minx_),
			calc_xyindex(goal_position.y, miny_),
			0.f,
			-1
	};
	return true;
}
//bool AstarNavigation::MakePlan() {
//}
bool AstarNavigation::calc_obstacle_map(std::vector<float> ox, std::vector<float> oy) {
	minx_ = -(Tx-1)*reso_;
	miny_ = -(Ty-1)*reso_;
	maxx_ = (W-Tx)*reso_;
	maxy_ = (H-Ty)*reso_;
	std::cout << "minxy,maxxy" << minx_ << " " << miny_ << " " << maxx_ << " "
			<< maxy_ << std::endl;
	xwidth_ = W;
	ywidth_ = H;
	std::cout << "xwidth,ywidth" << xwidth_ << " " << ywidth_ << std::endl;
	float x;
	float y;
	obmap.assign(xwidth_, std::vector<bool>(ywidth_, false));
	for (int iy = 0; iy < ywidth_; iy++) {
		y = calc_grid_position(iy, miny_);
		for (int ix = 0; ix < xwidth_; ix++) {
			x = calc_grid_position(ix, miny_);
			int index=W*iy+ix;
			for (uint io = 0; io < ox.size(); io++) {
				float d = hypotf(ox[io] - x, oy[io] - y);
				if (d <= rr_) {
					Data[index]=1;
					//obmap[iy][ix] = true;
					break;
				}
			}
		}
	}
	return true;
}


bool AstarNavigation::verify_node(Node node){
	float px=calc_grid_position(node.x,minx_);
	float py=calc_grid_position(node.y,miny_);
	if (px<minx_ || maxx_<=px) return false;
	if (py<miny_ || maxy_<=py) return false;
	//障害物map上ならfalse
	int index=W*node.y+node.x;
	if(Data[index]==1)return false;
	//if(obmap[node.y][node.x]) return false;
	//該当項目が全てなければOpenNodeに加える
	return true;
}

void AstarNavigation::MakeAsmap(){
	asmap.resize(xwidth_);
	for(int i=0;i<xwidth_;i++){
		asmap[i].resize(ywidth_);
	}
	for(int x=0;x<xwidth_;x++){
		for(int y=0;y<ywidth_;y++){
			asmap[y][x]=0;
		}
	}
}

bool AstarNavigation::MakePlan() {
	open_set[calc_grid_index(nstart_)] = nstart_;

	while (1) {
		if (open_set.size() == 0) {
			std::cout << "openset is empty" << std::endl;
			break;
		}
		int c_id;
		//std::cout << "set" << open_set.size() << "\n";
		c_id = get_minimum_key(ngoal_);
		Node current = open_set[c_id]; // @suppress("Invalid arguments")
		if (current.x == ngoal_.x && current.y == ngoal_.y) {
			cout << "find goal" << endl;
			ngoal_.pind = current.pind;
			ngoal_.cost = current.cost;
			break;
		}
		cout << "id" << c_id<<"x,y"<<current.x<<" "<<current.y;
		open_set.erase(c_id);
		//現在のコストが一番低いOpensetをClosedセットに加える
		closed_set[c_id] = current;
		Node node;
		for (uint i = 0; i < motion.size(); i++) {
			node = { (int) (current.x + motion[i][0]), (int) (current.y
					+ motion[i][1]), (float) (current.cost + motion[i][2]),
					(int) c_id };

			int n_id = calc_grid_index(node);
			//if the node id not safew do nothing
			if (!verify_node(node))
				continue;
			//closed setにすでにある場合
			if (closed_set.find(n_id) != closed_set.end()) {
				//すでにClosedに設定されている場合
				continue;
			}
			//open_nodeに設定されていない場合
			if (open_set.find(n_id) != closed_set.end()) {
				//新しいノードの追加
				open_set[n_id] = node;
			}
			//closed nodeにも設定されておらず、open nodeにはすでに設定されている場合。
			else {
				if (open_set[n_id].cost > node.cost) { // @suppress("Field cannot be resolved")
					//このPathが現在最適なPathとなる
					open_set[n_id] = node;
				}
			}
		}
	}
	calc_final_path(ngoal_, closed_set);
	return true;
}

int AstarNavigation::get_minimum_key(Node &ngoal){
	cout<< "set"<<open_set.size()<<"\n";
	float min_cost=INFINITY;
	float min_cost_key;
	int key;
	int cost;
	for(auto itr =open_set.begin();itr!=open_set.end();itr++){
		key=itr->first;
		cost=open_set[key].cost+calc_heuristic(ngoal,open_set[key]); // @suppress("Invalid arguments") // @suppress("Field cannot be resolved")
		if(min_cost>cost){
			min_cost=cost;
			min_cost_key=key;
		}
	}
	return min_cost_key;
}

//Done
void AstarNavigation::calc_final_path(Node ngoal, std::map<int,Node> closedset){
	rx_={calc_grid_position(ngoal.x,minx_)};
	ry_={calc_grid_position(ngoal.y,miny_)};
	int pind=ngoal.pind;
	while (pind!=-1){
		Node n=closed_set[pind]; // @suppress("Invalid arguments")
		rx_.emplace_back(calc_grid_position(n.x,minx_));
		ry_.emplace_back(calc_grid_position(n.y,miny_));
		pind=n.pind;
	}
}

void AstarNavigation::create_debug_map(std::vector<float> ix, std::vector<float> iy, std::vector <float> ox, std::vector<float> oy){
	for(int i=rx_.size()-1;i>=0;i--){
		cout<<rx_[i]<<""<<ry_[i]<<"\n";
	}
	cout<<"\n";
	//route map
	for(int i=0;i<ix.size();i++){
		int x=ix[i]-minx_;
		int y=iy[i]-miny_;
		asmap[y][x]=1;
	}
	std::cout<<"\n";
	//obstacle map
	for(int i=0;i<ox.size();i++){
		int x=ox[i]-minx_;
		int y=oy[i]-miny_;
		asmap[y][x]=5;
	}
	int sx=(start_position_.x-minx_)/reso_;
	int sy=(start_position_.y-miny_)/reso_;
	asmap[sy][sx]=9;

	for(int y=ywidth_-1;y>=0;y--){
		for(int x=0;x<xwidth_;x++){
			std::cout<<asmap[y][x];
		}
		std::cout<<"\n";
	}
	//usleep(1000);
	return;
}

bool AstarNavigation::ClearMap(){
	open_set.clear();
	closed_set.clear();
	asmap.clear();
	obmap.clear();
	return true;
}



