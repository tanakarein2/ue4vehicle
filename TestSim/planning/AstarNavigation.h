// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "define/core_define.h"
#include <iostream>
#include <vector>
#include <map>
using std::cout;
using std::endl;

struct Node
{
	int x;
	int y;
	float cost;
	int pind;

	Node(int x = 0, int y = 0, float cost = 0.0f, int pind = 0)
	{
		this->x = x;
		this->y = y;
		this->cost = cost;
		this->pind = pind;
	}
	void print()
	{
		printf("x:%d,y:%d,cost:%f,pind:%d\n", x, y, cost, pind);
	}
};

//verified
static std::vector<std::vector<float>> get_motion_model()
{
	//if(debug) cout<<"got motion"<<endl;
	std::vector<std::vector<float>> motion = {
		{1, 0, 1},
		{0, 1, 1},
		{-1, 0, 1},
		{0, -1, 1},
		{-1, -1, sqrt(2.0f)},
		{-1, 1, sqrt(2.0f)},
		{1, -1, sqrt(2.0f)},
		{1, 1, sqrt(2.0f)}};
	return motion;
}

static float calc_heuristic(Node &n1, Node &n2)
{
	float w = 1.0f;
	float d = w * hypot(n1.x - n2.x, n1.y - n2.y);
	return d;
}

class AstarNavigation{
private:
	Rect grid_range;
	bool debug=true;
	float reso_;
	float rr_;
	float minx_=0;
	float miny_=0;
	float maxx_=0;
	float maxy_=0;
	int xwidth_=0;
	int ywidth_=0;
	//gridの数は必ず奇数
	int W=31;
	//Hは必ず５の倍数
	int H=31;
	//0含む左から何番目
	int Tx=(W-1)/2+1;
	//0含む下から何番目
	int Ty=(H-1)/2+1;
	int sx_=0;
	int sy_=-9;
	int Data[31*31]={};


	Vector2i start_position_;
	Vector2i goal_position_;
	Node nstart_;
	Node ngoal_;

	std::vector<std::vector<float>> motion;
	std::map<int,Node> open_set;
	std::map<int,Node> closed_set;
	int calc_xyindex(float position,float min_pos);
	float calc_grid_position(int index,float minp);
	int calc_grid_index(Node node);

public:
	std::vector<float> rx_;
	std::vector<float> ry_;
	std::vector<std::vector<bool>> obmap;
	std::vector<std::vector<int>> asmap;

	AstarNavigation(float grid_size, float robot_radius);
	bool SetStartNode(Vector2i& transform);
	bool SetGoalNode(Vector2i& transform);
	bool calc_obstacle_map(std::vector<float> ox,std::vector<float> oy);
	void MakePath(Vector2i position,float angle){}
	bool ClearMap();
	bool MakePlan();
	int get_minimum_key(Node &ngoal);
	void calc_final_path(Node ngoal,std::map<int,Node> closedset);
	bool verify_node(Node node);

	void MakeAsmap();

	void create_debug_map(std::vector<float> ix,std::vector<float> iy,std::vector <float> ox,std::vector<float> oy);
};
