// Fill out your copyright notice in the Description page of Project Settings.


#include "PurePursuitControl.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"
#include <math.h>
#include <iostream>

PurePursuitControl::PurePursuitControl() : RADIUS_MAX_(9e10), KAPPA_MIN_(1 / RADIUS_MAX_)
//, is_linear_interpolation_(false)
//, next_waypoint_number_(-1)
		, lookahead_distance_(0), minimum_lookahead_distance_(6)
//, current_linear_velocity_(0)
		, lookahead_distance_ratio_(2.0) {
	std::cout << "ppc created " << std::endl;
}

double PurePursuitControl::calcCurvature(Vector3 target) const {
	double kappa;
	//double denominator = pow(getPlaneDistance(target, current_pose_.position), 2);
	////double numerator = 2 * calcRelativeCoordinate(target, current_pose_).y;
//
	//if (denominator != 0)
	//{
	//	kappa = numerator / denominator;
	//}
	//else
	//{
	//	if (numerator > 0)
	//	{
	//		kappa = KAPPA_MIN_;
	//	}
	//	else
	//	{
	//		kappa = -KAPPA_MIN_;
	//	}
	//}
	//ROS_INFO("kappa : %lf", kappa);
	return kappa;
}

double PurePursuitControl::GetSteerControl(){
	//lookahead_distance_=minimum_lookahead_distance_
	//Lf = k * state.v + Lfc
	//Vector2 pos2goal = {goal.x - geometry.pose.position.x,
	//				  goal.y - geometry.pose.position.y};
	const double WB = 2.0;
	lookahead_distance_ = lookahead_distance_ + lookahead_distance_ratio_ * ackermannDrive.speed;
	double alpha = atan2(target.y - position.y, target.x - position.x) - geometry.pose.euler.z;
	double delta = atan2(2.0 * WB * sin(alpha) / lookahead_distance_, 1.0);


	return delta;
}

PurePursuitControl::~PurePursuitControl() {
	std::cout << "del ppc" << endl;
}


void PurePursuitControl::Start() {
	//predictedLoci.header = {0, time(NULL), "dwa local path", 0.f};
}
void PurePursuitControl::SetState() {
	this->path = NavigationMsgs::instance()->globalPath;
	this->geometry = VehicleStateMsgs::instance()->geometry;
	ackermannDrive = AckermannMsgs::instance()->ackermannDrive;
}
void PurePursuitControl::SpinOnce() {
	controlCommand.steering_angle=GetSteerControl();

}
void PurePursuitControl::GetState() {
}