// Fill out your copyright notice in the Description page of Project Settings.

//refered to https://gitlab.com/autowarefoundation/autoware.ai/core_planning/-/blob/master/pure_pursuit/test/src/test_pure_pursuit.cpp

#pragma once

#include "design/TemplateMethod.h"
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/ackermann_define.h"
#include "define/ctrl_define.h"
/**
 * 
 */
class PurePursuitControl:public TemplateMethod
{
private:
	void SetState() override;
	void SpinOnce() override;
	void GetState() override;
	void Start() override;
	bool isDebug = true;

	//float k = 0.1f;//  # look forward gain
	//float Lfc = 2.0;//  # [m] look-ahead distance
	//float Kp = 1.0;//  # speed proportional gain
	//float dt = 0.1;//  # [s] time tick
	//float WB = 2.9f;//  # [m] wheel base of vehicle

	// constant
	const double RADIUS_MAX_;
	const double KAPPA_MIN_;

	// variables
	//bool is_linear_interpolation_;
	//int next_waypoint_number_;
	Vector3 next_target_position_;
	double lookahead_distance_;
	double minimum_lookahead_distance_;
	Pose current_pose_;
	double current_linear_velocity_;
	//std::vector<autoware_msgs::Waypoint> current_waypoints_;
	// functions
	double calcCurvature(Vector3 target) const;
	//bool interpolateNextTarget(
	//		int next_waypoint, geometry_msgs::Point* next_target) const;
	//void getNextWaypoint();

	//Original
	Vector3 target;
	Vector3 position;
	double GetSteerControl();

	Geometry geometry={};
	Path path;
	AckermannDrive ackermannDrive={};
	ControlCommand controlCommand;

	//Node
	double lookahead_distance_ratio_=0.5;

public:
	PurePursuitControl();
	~PurePursuitControl();
};
