//
// Created by tanakacar on 2020/04/09.
//

#include "DFS.h"


int DFS::Main() {
	// 高さ, 幅入力
	int H, W; cin >> H >> W; // 使わないけど一応入力

	// 街情報の入力
	vector<string> city(H);
	for (int i = 0; i < H; i++) cin >> city[i];

	DFS dfs;
	dfs.Initialize(city);
	llong depth = dfs.search();

	// 深さ優先探索の結果見つからなかったら
	if (depth == -1) {
		cout << "No" << endl;
	}
	else { // 見つかったら
		cout << "Yes" << endl;
	}

	return 0;
}