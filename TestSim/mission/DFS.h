//
// Created by tanakacar on 2020/04/09.
//

#ifndef TESTSIM_DFS_H
#define TESTSIM_DFS_H
#include <bits/stdc++.h>
#include "define/core_define.h"
#include "define/geom_define.h"
#include "define/nav_define.h"
#include "define/ackermann_define.h"
#include "define/ctrl_define.h"
using namespace std;
typedef long long llong;

// 深さ優先探索を行うクラス
class DFS {
private:
	Vector2i s;
	Vector2i g;
	
	int H, W; // city(街)の高さと幅
	int dx[4] = { -1, 0, 0, 1 }; // 注目座標から4近傍を探索するためのxの移動量
	int dy[4] = { 0, -1, 1, 0 }; // 注目座標から4近傍を探索するためのyの移動量

	struct Coordinate { // xy座標を表す
		int y; // y座標
		int x; // x座標
		int depth; // 深さ(スタート座標では0)
	};

	vector<string> m_city; // 街
	stack<Coordinate> st; // 座標とその深さを保持するスタック

public:
	int Main();
	void Initialize(vector<string> & city) {
		m_city = city;
		H = city.size(); // 街の高さ
		W = city[0].size(); // 街の幅

		// スタート座標(s.y,s.x)とゴール座標(g.y, g.x)を街から探索してセットする
		for (int y = 0; y < H; y++) {
			for (int x = 0; x < W; x++) {
				if (city[y][x] == 's') { // スタート地点
					s.y = y; // スタート地点のy座標
					s.x = x; // スタート地点のx座標
				}
				if (city[y][x] == 'g') { // ゴール地点
					g.y = y; // ゴール地点のy座標
					g.x = x; // ゴール地点のx座標
				}
			}
		}
	};

	// 深さ優先探索開始
	llong search() {
		// まずスタート地点をスタックに格納する
		Coordinate start = { s.y, s.x, 0 }; // y座標, x座標, 深さ(スタートは深さ0)
		st.push({ s.y, s.x, 0 });

		// スタックが空になる(完全にcity(街)を探索しきる)まで探索する
		while (!st.empty()) {
			// スタックの一番上を取ってポップする
			Coordinate now = st.top(); st.pop();

			// スタックの頂点がゴール座標であるならその時点での深さを返す
			if (now.y == g.y && now.x == g.x) return now.depth;

			// 注目してる座標の4近傍(上下左右)を探索
			for (int i = 0; i < 4; i++) {
				Coordinate next = { now.y + dy[i], now.x + dx[i], now.depth + 1 }; // 深さは+1する

				// 街の高さや幅を超える範囲の探索はしない(範囲外エラーになるから)
				if (next.y < 0 || H <= next.y) continue;
				if (next.x < 0 || W <= next.x) continue;

				// 壁だったら無視
				if (m_city[next.y][next.x] == '#') continue;

				// 既に探索済みなら無視
				if (m_city[next.y][next.x] == 'x') continue;

				// 探索済みの印(x)を入れておく
				m_city[next.y][next.x] = 'x';

				// スタックに次の座標と深さをプッシュしておく
				st.push(next);
			}
		}

		// 見つからなかったら-1でも返しておく
		return -1;
	}
};

#endif //TESTSIM_DFS_H
