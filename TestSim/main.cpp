#define _USE_MATH_DEFINES

#include "main.h"
#include <bits/stdc++.h>

#include "define/tf_define.h"
#include "define/ackermann_define.h"
#include "define/nav_define.h"
#include "define/math_define.h"
#include "msgs/sensor_msgs.h"
#include "msgs/ros_msgs.h"
#include "msgs/auto_msgs.h"


#include "planning/PurePursuitControl.h"
#include "mission/DFS.h"


using namespace std;



class Vehicle {
public:
	string name = "";
	AckermannDrive ackermannDrive = {};
	Geometry geometry = {};

	Vehicle(const string name, const AckermannDrive &ackermannDrive, const Geometry &geometry) {
		this->name = name;
		this->ackermannDrive = ackermannDrive;
		this->geometry = geometry;
	};
};



class TestClass {
public:
	TemplateMethod *ppc = new PurePursuitControl();
};

void Estimate(const Accel &accel, Twist &twist, Pose &pose) {
	float dt = 0.02f;
	twist.linear.x = accel.linear.x * dt;
	twist.linear.y = accel.linear.y * dt;
	twist.linear.z = accel.linear.z * dt;
	twist.angular.z = accel.angular.z * dt;

	pose.position.x = twist.linear.x * dt;
	pose.position.y = twist.linear.y * dt;
	pose.position.z = twist.linear.z * dt;
	pose.euler.z = accel.angular.z * dt;

	return;
}

void Estimate(Pose &pose, Twist &twist, AckermannDrive &ackermannDrive) {
	float angle = pose.euler.z;
	float phi = ackermannDrive.steering_angle;
	float v1 = ackermannDrive.speed;
	float v2 = ackermannDrive.steering_angle_velocity;
	float dt = 0.02f;
	const float l = 2.4f;
	pose.position.x += dt * v1 * cos(angle);
	pose.position.y += dt * v1 * sin(angle);
	pose.euler.z += dt * v1 * tan(phi) / l;
	return;
}


class VehicleStatus {
};


void passing() {
}

string SimpleScan(float x) {
	string fmbPos = "";
	const float FRONT_THRESH = 50.f;
	const float BACK_THRESH = -50.f;
	//Right side
	if (abs(x) < FRONT_THRESH) {
		fmbPos = "M";
	} else if (x > FRONT_THRESH) {
		fmbPos = "R";
	} else if (x < BACK_THRESH) {
		fmbPos = "L";
	} else {
	}
	return fmbPos;
}

string SimpleCulstering(float y) {

	string lmrPos = "";
	const float RIGHT_THRESH = -2.f;
	const float LEFT_THRESH = 2.f;
	//Right side
	if (abs(y) < RIGHT_THRESH) {
		lmrPos = "M";
	} else if (y > RIGHT_THRESH) {
		lmrPos = "R";
	} else if (y < LEFT_THRESH) {
		lmrPos = "L";
	}
	return lmrPos;
}


void Perception() {
	//
	//

}



void UnitTest::Main(){
	//Pose svPose = {Vector3(0.f, 0.f, 0.f), Vector3(0.f, 0.f, 0.f)};
	Pose svPose = {Vector3(0.f, 5.f, 0.f), Vector3(0.f, 0.f, M_PI_2)};

	Twist svTwist = {Twist(Vector3(0.f, 5.f, 0.f), Vector3())};
	Vehicle svCar = {"sv", AckermannDrive(), Geometry(svPose, Twist(Vector3(0.f, 4.f, 0.f), Vector3()), Accel())};

	Pose tlPose = {Vector3(2.f, 10.f, 0.f), Vector3(0.f, 0.f, M_PI_2)};

	Pose lvPose = {Vector3(2.f, 0.f, 0.f), Vector3(0.f, 0.f, M_PI_2)};
	Pose clPose = {Vector3(0.f, 10.f, 0.f), Vector3(0.f, 0.f, M_PI_2)};
	Pose flPose = {Vector3(0.f, 0.f, 0.f), Vector3(0.f, 0.f, M_PI_2)};

	Vehicle clCar = {"cl", AckermannDrive(), Geometry(clPose, Twist(Vector3(0.f, 5.f, 0.f), Vector3()), Accel())};
	Vehicle tlCar = {"tl", AckermannDrive(), Geometry(tlPose, Twist(), Accel())};
	Vehicle lvCar = {"lv", AckermannDrive(), Geometry(lvPose, Twist(), Accel())};
	Vehicle flCar = {"fl", AckermannDrive(), Geometry(flPose, Twist(), Accel())};

	//vehicles = {clCar, tlCar, lvCar, flCar};


	Vector3 clLocal = TF::World2LocalVector(svCar.geometry.pose, clCar.geometry.pose.position);
	Vector3 tlLocal = TF::World2LocalVector(svCar.geometry.pose, tlCar.geometry.pose.position);
	Vector3 clLocalTwist = TF::World2LocalTwist(svCar.geometry.twist.linear, clCar.geometry.twist.linear,
												svCar.geometry.pose.euler.z);

	PolarVector clPolar = TF::LocalCartesian2PolarVector(clLocal);
	PolarVector tlPolar = TF::LocalCartesian2PolarVector(tlLocal);

	PolarVelocity clPolarVelo = TF::LocalCartesian2PolarVelocity(clLocal, clLocalTwist);
	PolarVector clPolarVector = TF::World2LocalPolarVector(svCar.geometry.pose, clPose.position);
	PolarVelocity clPolarVelocity = TF::World2LocalPolarVelocity(svCar.geometry.pose, svCar.geometry.twist.linear,
																 clCar.geometry.pose.position,
																 clCar.geometry.twist.linear);

	//Recognition();
	clPolar.Debug("clPolar");
	clPolarVelo.Debug("cl polar velo");
	clPolarVelocity.Debug("cl polar velocity");
	tlPolar.Debug("tlPolar");
	clLocal.Debug("local Vector3 cl");
	tlLocal.Debug("local Vector3 tl");

	clLocalTwist.Debug("cltw");

	std::cout << "Hello, World!" << std::endl;

	DetectedObjectArray detectedObjectArray;
	DetectedObject tlObj,clObj,lvObj;
	tlObj.pose=tlPose;
	clObj.pose=clPose;
	lvObj.pose=lvPose;

	detectedObjectArray.objects.emplace_back(tlObj);
	detectedObjectArray.objects.emplace_back(clObj);
	detectedObjectArray.objects.emplace_back(lvObj);
	Geometry geometry;
	geometry.pose=svPose;
	SensorMsgs::instance()->detectedObjectArray=detectedObjectArray;
	VehicleStateMsgs::instance()->geometry=geometry;
	//rec.Update();
	rec->Update();

	ppc->Update();
}


int main() {

	UnitTest ut;
	DFS dfs;
	ut.Main();
	cout<<"unit Test"<<endl;
	dfs.Main();
	cout<<"dfs finished"<<endl;
	return 0;
}
