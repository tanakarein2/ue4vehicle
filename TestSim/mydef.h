//
// Created by tanaka-desk on 2020/03/26.
//

#ifndef UNTITLED1_MYDEF_H
#define UNTITLED1_MYDEF_H

#define _USE_MATH_DEFINES

#include "design/TemplateMethod.h"
#include "perception/Recognition.h"
#include <chrono> //for std clock
#include <cmath>
#include <iostream>
#include <map>
#include <math.h>
#include <random>
#include <vector>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::chrono::time_point;

#include <time.h>

class UnitTest
{
public:
	void Main();
	TemplateMethod *rec = new Recognition();
	//Recognition rec={};
};

#endif //UNTITLED1_MYDEF_H
