# 目標
このレポジトリはROSを使わないAutowareのパクリおよび劣化版を目指しています！

# codeing rule

- namespace rule : namespace_rule
- function rule : FunctionRule
- class rule : ClassRule
- class instance : classInstance
- menber variables : memberVariables
- constant value : CONSTANT_VALUE 

Upper camel case(アッパーキャメルケース) 	SampleTestData 	先頭大文字
Lower camel case(ロワーキャメルケース) 	sampleTestData 	先頭小文字
Screaming snake case(スクリーミングスネークケース) 	SAMPLE_TEST_DATA 	大文字
Snake case(スネークケース) 	sample_test_data 	小文字
Kebab case(ケバブケース) 	sample-test-data 	ハイフン区切り
Train case(トレインケース) 	Sample-Test-Data 	単語先頭が大文字


templete type T class U



# Design

## Template Method
TemplateMethod *exp=new ExampleClass();

## Singleton Template

```
template <class U>
class Singleton
{
public:
	static U *instance()
	{
		static U Instance;
		return &Instance;
	}
```

# meaning

- current t
- desired d
- sd coordinate p
- world coordinate w
- local<->world


# sequence

Update
1. Start(Initialized only)
2. GetState
3. SpinOnce
4. SetState


Tile tip
ffmpeg	-i a1.mpg -i a0.mpg -filter_complex "
		nullsrc=size=640x240 [base];
		[0:v] setpts=PTS-STARTPTS, scale=320x240 [left];
		[1:v] setpts=PTS-STARTPTS, scale=320x240 [right];
		[base][left] overlay=shortest=1 [tmp1];
		[tmp1][right] overlay=shortest=1:x=320
	" output.mp4

#　ここでCLIOｎパス設定
	// Linux Default Install
	if(FPaths::FileExists(TEXT("/home/tanaka-desk/clion-2019.3.5/bin")))
	{
		return TEXT("/home/tanaka-desk/clion-2019.3.5/bin");
	}
#endif

	// Nothing was found, return nothing as well
	return TEXT("");

# CLion

ここの数値を今回は、以下のように変更します


-Xms2G
-Xmx4G

- ![looks like](https://gitlab.com/tanakarein2/ue4vehicle/-/blob/master/unreal.png)